using System;
using System.IO;
using System.Linq;

namespace BetterPractices
{
    /// <summary>
    /// This is a class designed around practices that are generally bad.
    /// </summary>
    public static class BadPractice
    {
        public static void EncryptFileToday(string path)
        {
            if (!(path == null || path.Equals(string.Empty)))
            {
                if (!(Path.GetInvalidPathChars().Any(path.Contains) || Path.GetInvalidPathChars().Any(path.Contains)))
                {
                    var file = new FileInfo(path);
                    if (!(file.LastWriteTime > DateTime.Today.Date))
                    {
                        if (!file.Exists)
                        {
                            Console.WriteLine("Error: File does not exist!");
                            Console.WriteLine("Alert: Creating file.");
                            file.Create();
                            Console.WriteLine("Alert: Created File: " + file.Name);
                        }
                        Console.WriteLine("Alert: Encrypting File: " + file.Name);
                        file.Encrypt();
                    }
                    else
                    {
                        Console.WriteLine("Error: File already saved today!");
                    }
                }
                else
                {
                    Console.WriteLine("Error: Invalid path!");
                }
            }
            else
            {
                Console.WriteLine("Error: Empty file!");
            }
        }

        public static bool EncryptFileTodayAndRespond(string path)
        {
            if (!(path == null || path.Equals(string.Empty)))
            {
                if (!(Path.GetInvalidPathChars().Any(path.Contains) || Path.GetInvalidPathChars().Any(path.Contains)))
                {
                    var file = new FileInfo(path);
                    if (!(file.LastWriteTime > DateTime.Today.Date))
                    {
                        if (!file.Exists)
                        {
                            Console.WriteLine("Error: File does not exist!");
                            Console.WriteLine("Alert: Creating file.");
                            file.Create();
                            Console.WriteLine("Alert: Created File: " + file.Name);
                        }
                        Console.WriteLine("Alert: Encrypting File: " + file.Name);
                        file.Encrypt();
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Error: File already saved today!");
                        return false;
                    }
                }
                else
                {
                    Console.WriteLine("Error: Invalid path!");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("Error: Empty file!");
                return false;
            }
        }

        public static void EncryptFileTodayAndRespondWithExceptions(string path)
        {
            if (!(path == null || path.Equals(string.Empty)))
            {
                if (!(Path.GetInvalidPathChars().Any(path.Contains) || Path.GetInvalidPathChars().Any(path.Contains)))
                {
                    var file = new FileInfo(path);
                    if (!(file.LastWriteTime > DateTime.Today.Date))
                    {
                        if (!file.Exists)
                        {
                            Console.WriteLine("Error: File does not exist!");
                            Console.WriteLine("Alert: Creating file.");
                            file.Create();
                            Console.WriteLine("Alert: Created File: " + file.Name);
                        }
                        Console.WriteLine("Alert: Encrypting File: " + file.Name);
                        file.Encrypt();
                    }
                    else
                    {
                        Console.WriteLine("Error: File already saved today!");
                        throw new Exception("Error: File already saved today!");
                    }
                }
                else
                {
                    Console.WriteLine("Error: Invalid path!");
                    throw new Exception("Error: Invalid path!");
                }
            }
            else
            {
                Console.WriteLine("Error: Empty file!");
                throw new Exception("Error: Empty file!");
            }
        }
    }
}