﻿using System;
using System.IO;
using System.Linq;

namespace BetterPractices
{
    /// <summary>
    /// This is a class designed around practices that are generally better.
    /// </summary>
    public static class BetterPractice
    {
        public static void EncryptFileToday(string path)
        {
            if (path == null || path.Equals(string.Empty))
            {
                Console.WriteLine("Error: Empty file!");
                return;
            }

            if (Path.GetInvalidPathChars().Any(path.Contains) || Path.GetInvalidPathChars().Any(path.Contains))
            {
                Console.WriteLine("Error: Invalid path!");
                return;
            }

            var file = new FileInfo(path);

            if (file.LastWriteTime > DateTime.Today.Date)
            {
                Console.WriteLine("Error: File already encrypted today!");
                return;
            }

            if (!file.Exists)
            {
                Console.WriteLine("Error: File does not exist!");
                Console.WriteLine("Alert: Creating file.");
                file.Create();
                Console.WriteLine("Alert: Created File: " + file.Name);
            }

            Console.WriteLine("Alert: Encrypting File: " + file.Name);
            file.Encrypt();
        }

        public static bool EncryptFileTodayAndRespond(string path)
        {
            if (path == null || path.Equals(string.Empty))
            {
                Console.WriteLine("Error: Empty file!");
                return false;
            }

            if (Path.GetInvalidPathChars().Any(path.Contains) || Path.GetInvalidPathChars().Any(path.Contains))
            {
                Console.WriteLine("Error: Invalid path!");
                return false;
            }

            var file = new FileInfo(path);

            if (file.LastWriteTime > DateTime.Today.Date)
            {
                Console.WriteLine("Error: File already encrypted today!");
                return false;
            }

            if (!file.Exists)
            {
                Console.WriteLine("Error: File does not exist!");
                Console.WriteLine("Alert: Creating file.");
                file.Create();
                Console.WriteLine("Alert: Created File: " + file.Name);
            }

            Console.WriteLine("Alert: Encrypting File: " + file.Name);
            file.Encrypt();
            return true;
        }

        public static void EncryptFileTodayAndRespondWithExceptions(string path)
        {
            if (path == null || path.Equals(string.Empty))
            {
                Console.WriteLine("Error: Empty file!");
                throw new Exception("Error: Empty file!");
            }

            if (Path.GetInvalidPathChars().Any(path.Contains) || Path.GetInvalidPathChars().Any(path.Contains))
            {
                Console.WriteLine("Error: Invalid path!");
                throw new Exception("Error: Invalid path!");
            }

            var file = new FileInfo(path);

            if (file.LastWriteTime > DateTime.Today.Date)
            {
                Console.WriteLine("Error: File already encrypted today!");
                throw new Exception("Error: File already encrypted today!");
            }

            if (!file.Exists)
            {
                Console.WriteLine("Error: File does not exist!");
                Console.WriteLine("Alert: Creating file.");
                file.Create();
                Console.WriteLine("Alert: Created File: " + file.Name);
            }

            Console.WriteLine("Alert: Encrypting File: " + file.Name);
            file.Encrypt();
        }
    }
}